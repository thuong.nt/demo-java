package javaBasic.example.restApi.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class CustomerModel {
    public String client_id;
    public boolean accepts_marketing;
    public Timestamp birthday;
    public Timestamp created_on;
    public long default_address_id;
    public String email;
    public String first_name;
    public String last_name;
    public String last_order_id;
    public String last_order_name;
    public String member_ship;
    public long loyalty_point;
    public long modified_on;
    public long order_count;
    public String note;
    public String phone;
    public ArrayList<AddressModel> addresses;
    public void setClientId(String clientId) {
        this.client_id = clientId;
    }

    public String sex;

    public String getClientId() {
        return client_id;
    }

    public boolean getAccepts_marketing() {
        return accepts_marketing;
    }

    public Timestamp getBirthday() {
        return birthday;
    }

    public String state;
    public String store_last_order_name;
    public String store_last_name;
    public long store_order_count;
    public long store_total_spent;

    public ArrayList<AddressModel> getAddresses() {
        return addresses;
    }

    public void setAddresses(ArrayList<AddressModel> addresses) {
        this.addresses = addresses;
    }

    public String tag_aliases;
    public String tags;
    public long total_spent;
    public String zalo_id;
}
