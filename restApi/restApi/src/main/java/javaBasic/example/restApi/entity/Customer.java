package javaBasic.example.restApi.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;

@Entity
public class Customer {
    @Id @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    public String client_id;
    public boolean accepts_marketing;
    public Timestamp birthday;
    public Timestamp created_on;
    public long default_address_id;
    public String email;
    public String first_name;
    public String last_name;
    public String last_order_id;
    public String last_order_name;
    public String member_ship;
    public long loyalty_point;
    public long modified_on;
    public long order_count;
    public String note;
    public String phone;
    public String sex;
    public String state;
    public String store_last_order_name;
    public String store_last_name;
    public long store_order_count;
    public long store_total_spent;
    public String tag_aliases;
    public String tags;
    public long total_spent;
    public String zalo_id;
    public ArrayList<String> address_ids;

    public Customer() {
    }

    public Customer(String client_id, boolean accepts_marketing, Timestamp birthday, Timestamp created_on, long default_address_id, String email, String first_name, String last_name, String last_order_id, String last_order_name, String member_ship, long loyalty_point, long modified_on, long order_count, String note, String phone, String sex, String state, String store_last_order_name, String store_last_name, long store_order_count, long store_total_spent, String tag_aliases, String tags, long total_spent, String zalo_id, ArrayList<String> address_ids) {
        this.client_id = client_id;
        this.accepts_marketing = accepts_marketing;
        this.birthday = birthday;
        this.created_on = created_on;
        this.default_address_id = default_address_id;
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
        this.last_order_id = last_order_id;
        this.last_order_name = last_order_name;
        this.member_ship = member_ship;
        this.loyalty_point = loyalty_point;
        this.modified_on = modified_on;
        this.order_count = order_count;
        this.note = note;
        this.phone = phone;
        this.sex = sex;
        this.state = state;
        this.store_last_order_name = store_last_order_name;
        this.store_last_name = store_last_name;
        this.store_order_count = store_order_count;
        this.store_total_spent = store_total_spent;
        this.tag_aliases = tag_aliases;
        this.tags = tags;
        this.total_spent = total_spent;
        this.zalo_id = zalo_id;
        this.address_ids = address_ids;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public boolean isAccepts_marketing() {
        return accepts_marketing;
    }

    public void setAccepts_marketing(boolean accepts_marketing) {
        this.accepts_marketing = accepts_marketing;
    }

    public Timestamp getBirthday() {
        return birthday;
    }

    public void setBirthday(Timestamp birthday) {
        this.birthday = birthday;
    }

    public Timestamp getCreated_on() {
        return created_on;
    }

    public void setCreated_on(Timestamp created_on) {
        this.created_on = created_on;
    }

    public long getDefault_address_id() {
        return default_address_id;
    }

    public void setDefault_address_id(long default_address_id) {
        this.default_address_id = default_address_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getLast_order_id() {
        return last_order_id;
    }

    public void setLast_order_id(String last_order_id) {
        this.last_order_id = last_order_id;
    }

    public String getLast_order_name() {
        return last_order_name;
    }

    public void setLast_order_name(String last_order_name) {
        this.last_order_name = last_order_name;
    }

    public String getMember_ship() {
        return member_ship;
    }

    public void setMember_ship(String member_ship) {
        this.member_ship = member_ship;
    }

    public long getLoyalty_point() {
        return loyalty_point;
    }

    public void setLoyalty_point(long loyalty_point) {
        this.loyalty_point = loyalty_point;
    }

    public long getModified_on() {
        return modified_on;
    }

    public void setModified_on(long modified_on) {
        this.modified_on = modified_on;
    }

    public long getOrder_count() {
        return order_count;
    }

    public void setOrder_count(long order_count) {
        this.order_count = order_count;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStore_last_order_name() {
        return store_last_order_name;
    }

    public void setStore_last_order_name(String store_last_order_name) {
        this.store_last_order_name = store_last_order_name;
    }

    public String getStore_last_name() {
        return store_last_name;
    }

    public void setStore_last_name(String store_last_name) {
        this.store_last_name = store_last_name;
    }

    public long getStore_order_count() {
        return store_order_count;
    }

    public void setStore_order_count(long store_order_count) {
        this.store_order_count = store_order_count;
    }

    public long getStore_total_spent() {
        return store_total_spent;
    }

    public void setStore_total_spent(long store_total_spent) {
        this.store_total_spent = store_total_spent;
    }

    public String getTag_aliases() {
        return tag_aliases;
    }

    public void setTag_aliases(String tag_aliases) {
        this.tag_aliases = tag_aliases;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public long getTotal_spent() {
        return total_spent;
    }

    public void setTotal_spent(long total_spent) {
        this.total_spent = total_spent;
    }

    public String getZalo_id() {
        return zalo_id;
    }

    public void setZalo_id(String zalo_id) {
        this.zalo_id = zalo_id;
    }

    public ArrayList<String> getAddress_ids() {
        return address_ids;
    }

    public void setAddress_ids(ArrayList<String> address_ids) {
        this.address_ids = address_ids;
    }
}
