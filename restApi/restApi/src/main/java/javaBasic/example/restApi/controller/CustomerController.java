package javaBasic.example.restApi.controller;

import javaBasic.example.restApi.entity.Customer;
import javaBasic.example.restApi.entity.CustomerParam;
import javaBasic.example.restApi.model.CustomerModel;
import javaBasic.example.restApi.repository.CustomerRepository;
import javaBasic.example.restApi.response.CustomerResponse;
import javaBasic.example.restApi.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/customer")
public class CustomerController {
    @Autowired()
    private CustomerService customerService;
    @PostMapping("")
    public CustomerResponse createCustomer(@RequestBody CustomerModel customerModel){
        CustomerResponse c = customerService.createCustomer(customerModel);
        return c;
    }
    @PutMapping("/{id}")
    public CustomerResponse updateCustomer(@RequestBody CustomerModel customerModel,@PathVariable String id){
        CustomerResponse c = customerService.updateCustomerById(customerModel,id);
        return c;
    }
    @GetMapping("")
    public List<CustomerResponse> filterCustomer(@RequestParam Map<String,String> mapping){
        CustomerParam customerParam = new CustomerParam(mapping.get("name"),mapping.get("limit"),mapping.get("storeId"),mapping.get("page"),mapping.get(("merchantId")),mapping.get("email"));
        List<CustomerResponse> customerResponses = customerService.filterCustomer(customerParam);
        return customerResponses;
    }
}
