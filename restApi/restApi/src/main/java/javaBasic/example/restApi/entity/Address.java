package javaBasic.example.restApi.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class Address {
    public String address;
    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    public String client_id;
    public String country;
    public String country_code;
    public String district;
    public String district_code;
    public Timestamp create_one;
    public Timestamp modified_on;
    public String province;
    public String province_code;
    public String ward;
    public String zip_code;

}
