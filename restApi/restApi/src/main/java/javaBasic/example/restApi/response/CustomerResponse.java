package javaBasic.example.restApi.response;

import java.sql.Timestamp;

public class CustomerResponse {
    public String client_id;
    public String accepts_marketing;
    public Timestamp birthday;
    public Timestamp created_on;
    public long default_address_id;
    public String email;
    public String first_name;
    public String last_name;
    public String last_order_id;
    public String last_order_name;
    public String member_ship;
    public long loyalty_point;
    public long modified_on;
    public long order_count;
    public String note;
    public String phone;
    public String sex;
    public String state;
    public String store_last_order_name;
    public String store_last_name;
    public long store_order_count;
    public long store_total_spent;
    public String tag_aliases;
    public String tags;
    public long total_spent;
    public String zalo_id;
    public String address_ids;
}
